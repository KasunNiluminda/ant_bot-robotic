#include <Servo.h>

double usdata[12];
int slocdata[12];

Servo servo1;
Servo servo2;
Servo servo3;
Servo servo4;

void setup() {
  pinMode(12, INPUT);
  pinMode(11, OUTPUT);
  pinMode (3, OUTPUT);
  pinMode (4, OUTPUT);
  pinMode (5, OUTPUT);
  pinMode (6, OUTPUT);
  pinMode (10, OUTPUT);
  pinMode (9, OUTPUT);

  pinMode (A7, INPUT);
  pinMode (A5, INPUT);
  pinMode (A1, OUTPUT);
  pinMode (A2, OUTPUT);
  pinMode (A3, OUTPUT);
  pinMode (A4, OUTPUT);
  Serial.begin(9600);

  servo1.attach(A1);
  servo2.attach(A2);
  servo3.attach(A3);
  servo4.attach(A4);

  servo1.write(90);
  servo2.write(80);
  servo3.write(30);
  servo4.write(90);
  delay(5000);

}
int echo = 12;
int trig = 11;


int m2_1 = 9;
int m2_2 = 10;

int m1_1 = 3;
int m1_2 = 4;

int floc = 0;
int lloc = 0;

int olen;
boolean catchob = false;
void loop() {
  if (catchob) {
    for (int i = 0; i < 25 ; i++) {
      motor(-150 , 150);
      delay(30);
      motor(0, 0);
      delay(50);
    }
    for (int i = 0; i < 20 ; i++) {
      motor(150 , 150);
      delay(30);
      motor(0, 0);
      delay(50);
    }
    unload();
  } else {
    lroditect();
  }


}

int steps = 0;
int sensestep = 0;
void lroditect() {
  while (odistance() > 50) {
    motor(150, -150);
    delay(30);
    motor(0, 0);
    delay(50);
    steps = steps + 1;
    Serial.println(steps);
    if (steps > 102 || steps == 102) {
      for (int i = 0; i < 20; i++) {
        motor(150, 150);
        delay(30);
        motor(0, 0);
        delay(50);

      }
      steps = 0;
    }
  }

  double odis = odistance() + 5;

  while (odistance() < odis) {
    motor(150, -150);
    delay(30);
    motor(0, 0);
    delay(50);
    sensestep = sensestep + 1;
  }
  for (int i = 0; i < ((sensestep / 2)); i++) {
    motor(-150, 150);
    delay(30);
    motor(0, 0);
    delay(50);
  }
  if (odistance() > 30) {
    while (true) {
      motor(150, 150);
      delay(10);
      motor(0, 0);
      delay(30);
      if (odistance() > 20 && odistance() < 30) {
        motor(0, 0);
        break;
      }
    }
  }
  motor(0, 0);
  checko();

}

void checko() {
  double dis = odistance();
  if ( dis > 22) {
    while (odistance() > 21) {
      motor(150, 150);
      delay(10);
      motor(0, 0);
      delay(50);
    }
  }
  if ( dis < 22) {
    while (odistance() < 21) {
      motor(-150, -150);
      delay(10);
      motor(0, 0);
      delay(50);
    }
  }
  for (int q = 0; q < 2; q++) {
    int sloc = 120;
    servo1.write(sloc);
    delay(500);
    for (int i = 0; i < 12; i++) {
      servo1.write(sloc);
      delay(50);
      slocdata[i] = sloc;
      sloc = sloc - 5;
      usdata[i] = odistance();
      delay(20);
    }
  }

  for (int i = 1; i < 12; i++) {
    if (usdata[i] < 25) {
      if (floc == 0) {
        floc = 0;
        floc = slocdata[i];
      } else {
        lloc = slocdata[i];
      }
    }
  }
  olen = (floc - lloc) / 5;
  int servoloc = floc - ((olen * 5) / 2) + 1;
  servo1.write(servoloc - 1);
  delay(1000);

  for (int i = 80; i < 150 ; i++) {
    servo2.write(i);
    delay(30);
  }

  for (int i = 30; i < 100 ; i++) {
    servo3.write(i);
    delay(30);
  }

  while (true) {
    if (digitalRead(A5) == 1) {
      servo1.write(servoloc + 4);
      delay(15);
      break;

    } else {

      servoloc = servoloc + 1;
      servo1.write(servoloc);
      delay(100);
    }
  }

  for (int i = 150 ; i < 175; i++) {
    servo2.write(i);
    delay(30);
  }
  for (int i = 110 ; i > 87; i--) {
    servo3.write(i);
    delay(30);
  }

  while (true) {
    if (analogRead(A7) == 0) {
      digitalWrite(5, LOW);
      digitalWrite(6, LOW);
      break;
    }
    else {
      digitalWrite(5, LOW);
      digitalWrite(6, HIGH);
    }
  }

  digitalWrite(5, LOW);
  digitalWrite(6, HIGH);
  delay(100);
  digitalWrite(5, LOW);
  digitalWrite(6, LOW);

  for (int i = 90; i > 30 ; i--) {
    servo3.write(i);
    delay(15);
  }

  for (int i = 175; i > 80; i--) {
    servo2.write(i);
    delay(15);
  }

  servo1.write(90);
  delay(500);




  catchob = true;

}

void unload () {
  for (int i = 80; i < 180 ; i++) {
    servo2.write(i);
    servo3.write(i / 2);

    delay(30);
  }
  digitalWrite(5, HIGH);
  digitalWrite(6, LOW);
  delay(8500);
  digitalWrite(5, LOW);
  digitalWrite(6, LOW);

  for (int i = 90; i > 40 ; i--) {
    servo3.write(i);
    delay(15);
  }

  for (int i = 180; i > 80; i--) {
    servo2.write(i);
    delay(15);
  }

  servo1.write(90);
  delay(5000);
  catchob = false;
}






double odistance() {
  double d, t;
  digitalWrite(trig, LOW);
  delayMicroseconds(2);
  digitalWrite(trig, HIGH);
  delayMicroseconds(10);

  t = pulseIn(echo, HIGH);
  d = (t / 2) / 29.1;

  delay(300);
  return d;
}
void motor (int m1, int m2) {
  if (m1 > 0) {
    digitalWrite(m1_1, LOW);
    digitalWrite(m1_2, HIGH);
  } else if (m1 < 0) {
    digitalWrite(m1_1, HIGH);
    digitalWrite(m1_2, LOW);
  }
  if (m2 > 0) {
    digitalWrite(m2_1, LOW);
    digitalWrite(m2_2, HIGH);
  } else if (m2 < 0) {
    digitalWrite(m2_1, HIGH);
    digitalWrite(m2_2, LOW);
  }
  if (m1 == 0 && m2 == 0) {
    digitalWrite(m1_1, LOW);
    digitalWrite(m1_2, LOW);
    digitalWrite(m2_1, LOW);
    digitalWrite(m2_2, LOW);
  }
}
